const express = require('express');
const Album = require('../models/Album');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const User = require("../models/User");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const token = req.get('Authorization');

		const user = await User.findOne({token});

		const query = {};

		if (req.query.artist) {
			query.artist = req.query.artist;
		}

		const albums = await Album.find(query).populate('artist', 'name').sort('outYear');

		if (albums.length !== 0) {
			if (user && (user.role === 'admin')) {
				return res.send(albums);
			}

			if (!token || !user) {
				return res.send(albums.filter(album => album.published !== false));
			}

			return res.send(albums.filter(album => album.published !== false));

		} else {
			return res.status(404).send({error: 'Albums not fount', artist: await Artist.findById(query.artist)});
		}
	} catch (e) {
		res.sendStatus(500);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const album = await Album.findById(req.params.id).populate('artist', 'name');

		if (album) {
			res.send(album);
		} else {
			res.status(404).send({error: 'Album not found'});
		}

	} catch  {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	try {
		const albumData = {
			user: req.user._id,
			name: req.body.name,
			artist: req.body.artist,
			outYear: req.body.outYear,
			year: req.body.year
		};

		if (req.file) {
			albumData.image = 'uploads/' + req.file.filename;
		}

		const album = new Album(albumData);

		await album.save();
		res.send(album);
	} catch (error) {
		res.status(400).send(error);
	}

});

router.put('/:id/publish', auth, async (req, res) => {
	try {
		if (req.user.role !== 'admin') {
			return res.status(403).send({message: 'No permission'});
		}

		const album = await Album.findById(req.params.id);

		if (!album) {
			return res.status(404).send({message: 'Album not found!'});
		}

		const updateAlbum = await Album.findByIdAndUpdate(req.params.id, {published: true}, {
			new: true,
			runValidators: true,
		});

		res.send(updateAlbum);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	if (req.user.role !== 'admin') {
		return res.status(403).send({error: 'No permission'});
	}

	const albumById = await Album.findById(req.params.id);

	if (!albumById) {
		return res.status(404).send({error: 'Album not found'})
	}

	try {
		const album = await Album.findByIdAndRemove(req.params.id);

		res.send({message: 'Album deleted', album});
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;