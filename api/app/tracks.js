const express = require('express');
const Track = require('../models/Track');
const Artist = require('../models/Artist');
const Album = require('../models/Album');
const auth = require("../middleware/auth");
const User = require("../models/User");

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const token = req.get('Authorization');

		const user = await User.findOne({token});

		const query = {};

		if (req.query.album) {
			query.album = req.query.album;
		}

		const tracks = await Track.find(query).populate('album', 'name').sort("trackNumber");

		if (tracks.length !== 0) {
			if (!token || !user) {
				return res.send(tracks.filter(track => track.published !== false));
			}

			if (user && (user.role === 'admin')) {
				return res.send(tracks);
			}

			return res.send(tracks.filter(track => track.published !== false));
		} else {
			const album = await Album.findById(query.album);

			return res.status(404).send({
				error: 'Tracks not found',
				album: album,
				artist: await Artist.findById(album.artist)
			});
		}
	} catch (e) {
		res.sendStatus(500);
	}
});

router.post('/', auth, async (req, res) => {
	try {
		const trackData = {
			user: req.user._id,
			name: req.body.name,
			album: req.body.album,
			duration: req.body.duration || null,
			trackNumber: req.body.trackNumber,
		};

		const track = new Track(trackData);

		await track.save();
		res.send(track);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.put('/:id/publish', auth, async (req, res) => {
	try {
		if (req.user.role !== 'admin') {
			return res.status(403).send({message: 'No permission'});
		}

		const track = await Track.findById(req.params.id);

		if (!track) {
			return res.status(404).send({message: 'Track not found!'});
		}

		const updateTrack= await Track.findByIdAndUpdate(req.params.id, {published: true}, {
			new: true,
			runValidators: true,
		});

		res.send(updateTrack);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	const trackById = await Track.findById(req.params.id);

	if (!trackById) {
		return res.status(404).send({error: 'Track not found'})
	}

	if (req.user.role !== 'admin') {
		return res.status(403).send({error: 'No permission'});
	}
	try {
		const track = await Track.findByIdAndRemove(req.params.id);

		res.send({message: 'Track deleted', track});
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;