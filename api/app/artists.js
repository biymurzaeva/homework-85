const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const Artist = require('../models/Artist');
const {nanoid} = require("nanoid");
const auth = require("../middleware/auth");
const User = require("../models/User");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
	try {
		const token = req.get('Authorization');

		const user = await User.findOne({token});

		const artists = await Artist.find({published: true});

		if (artists.length !== 0) {
			const allArtists = await Artist.find();

			if (user && (user.role === 'admin')) {
				return res.send(allArtists);
			}

			if (!token || !user) {
				return res.send(artists);
			}

			return res.send(artists);

		} else {
			res.status(404).send({error: 'Artists not found'});
		}

	} catch (e) {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	try {
		const artistData = {
			user: req.user._id,
			name: req.body.name
		};

		if (req.file) {
			artistData.image = 'uploads/' + req.file.filename;
		}

		if (req.body.info) {
			artistData.info = req.body.info;
		}

		const artist = new Artist(artistData);

		await artist.save();
		res.send(artist);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.put('/:id/publish', auth, async (req, res) => {
	try {
		if (req.user.role !== 'admin') {
			return res.status(403).send({message: 'No permission'});
		}

		const artist = await Artist.findById(req.params.id);

		if (!artist) {
			return res.status(404).send({message: 'Artist not found!'});
		}

		const updateArtist = await Artist.findByIdAndUpdate(req.params.id, {published: true}, {
			new: true,
			runValidators: true,
		});

		res.send(updateArtist);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	if (req.user.role !== 'admin') {
		return res.status(403).send({error: 'No permission'});
	}

	const artistById = await Artist.findById(req.params.id);

	if (!artistById) {
		return res.status(404).send({error: 'Artist not found'})
	}

	try {
		const artist = await Artist.findByIdAndRemove(req.params.id);

		res.send({message: 'Artist deleted', artist});
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;