const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require("../middleware/auth");

const router = express.Router();

router.get('/', auth, async (req, res) => {
	try {
		const tracks = await TrackHistory.find({user_id: req.user._id}).populate({
			path: 'track_id',
			select: 'name album',
			populate: [{path: 'album', select: 'artist', populate: {path:'artist', select: 'name',}}],
		}).sort("datetime");

		if (tracks.length !== 0) {
			res.send(tracks);
		} else {
			res.status(404).send({error: 'Tracks not found'});
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/', auth, async (req, res) => {
	const trackHistoryData = {
		user_id: req.user._id,
		track_id: req.body.track_id,
		datetime: new Date().toISOString()
	};

	const trackHistory = new TrackHistory(trackHistoryData);

	try {
		await trackHistory.save();
		res.send(trackHistory);
	} catch (e) {
		res.sendStatus(500);
	}
});

module.exports = router;