const express = require('express');
const User = require('../models/User');
const path = require("path");
const multer = require("multer");
const {nanoid} = require("nanoid");
const config = require("../config");
const axios = require("axios");

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

router.post('/', upload.single('avatarImage'), async (req, res) => {
	try {
		const userData = {
			email: req.body.email,
			password: req.body.password,
			displayName: req.body.displayName,
		};

		if (req.file) {
			userData.avatarImage = 'uploads/' + req.file.filename;
		}

		const user = new User(userData);

		user.generateToken();
		await user.save();
		res.send(user);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/sessions', async (req, res) => {
	const user = await User.findOne({email: req.body.email});

	if (!user) {
		return res.status(401).send({message: 'Credentials are wrong'});
	}

	const isMatch = await user.checkPassword(req.body.password);

	if (!isMatch) {
		return res.status(401).send({message: 'Credentials are wrong'});
	}

	try {
		user.generateToken();
		await user.save({validateBeforeSave: false});

		res.send({message: 'Email and password correct!', user});
	} catch (e) {
		res.status(400).send(e);
	}
});

router.post('/facebookLogin', async (req, res) => {
	const inputToken = req.body.accessToken;

	const accessToken = config.facebook.addId + '|' + config.facebook.appSecret;

	const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

	try {
		const response = await axios.get(debugTokenUrl);

		if (response.data.data.error) {
			return res.status(401).send({global: 'Facebook token incorrect'});
		}

		if (req.body.id !== response.data.data.user_id) {
			return res.status(401).send({global: 'Wrong user ID'});
		}

		let user = await User.findOne({email: req.body.email});

		if (!user) {
			user = await User.findOne({email: req.body.id});
		}

		if (!user) {
			user = new User({
				email: req.body.email || req.body.id,
				password: nanoid(),
				displayName: req.body.name,
		    avatarImage: req.body.picture.data.url
			});
		}

		user.generateToken();
		await user.save({validateBeforeSave: false});
		res.send({message: 'Success!', user});
	} catch (error) {
		res.status(401).send({global: 'Facebook token incorrect'});
	}
});

router.delete('/sessions', async (req, res) => {
	const token = req.get('Authorization');
	const success = {message: 'Success'};

	if (!token) return res.send(success);

	const user = await User.findOne({token});

	if (!user) return res.send(success);

	user.generateToken();

	await user.save({validateBeforeSave: false});

	return res.send(success);
});

module.exports = router;