const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	name: {
		type: String,
		required: true,
	},
	artist: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Artist',
		required: true
	},
	outYear: {
		type: Number,
		required: true,
	},
	year: String,
	image: String,
	published: {
		type: Boolean,
		required: true,
		default: false,
		enum: [true, false],
	},
});

AlbumSchema.plugin(idvalidator);
const Album = mongoose.model('Album', AlbumSchema);
module.exports = Album;

