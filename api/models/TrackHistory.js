const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TrackHistorySchema = new mongoose.Schema({
	user_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	track_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Track',
		required: true
	},
	datetime: {
		type: String,
	}
});

TrackHistorySchema.plugin(idValidator);
const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);
module.exports = TrackHistory;