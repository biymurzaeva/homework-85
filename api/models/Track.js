const mongoose = require('mongoose');
const idvalidator = require('mongoose-id-validator');

const TrackSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	name: {
		type: String,
		required: true
	},
	album: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Album',
		required: true
	},
	duration: String,
	trackNumber: {
		type: Number,
		required: true,
	},
	published: {
		type: Boolean,
		required: true,
		default: false,
		enum: [true, false],
	},
});

TrackSchema.plugin(idvalidator);
const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;

