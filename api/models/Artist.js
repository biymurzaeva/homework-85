const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true,
	},
	name: {
		type: String,
		required: true,
		unique: true
	},
	image: String,
	info: String,
	published: {
		type: Boolean,
		required: true,
		default: false,
		enum: [true, false]
	},
});

const Artist = mongoose.model('Artist', ArtistSchema);
module.exports = Artist;