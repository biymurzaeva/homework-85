const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Artist = require('./models/Artist');
const User = require('./models/User');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async  () => {
	await mongoose.connect(config.db.url);

	const collections = await mongoose.connection.db.listCollections().toArray();

	for (const coll of collections) {
		await mongoose.connection.db.dropCollection(coll.name);
	}

	const [admin, user] = await User.create({
		email: 'admin@music.com',
		password: 'admin',
		token: nanoid(),
		role: 'admin',
		displayName: 'Admin',
	}, {
		email: 'user@music.com',
		password: 'user',
		token: nanoid(),
		role: 'user',
		displayName: 'User',
	});

	const [Ariana, Puth] = await Artist.create({
		user: user,
		name: 'Ariana Grande',
		image: 'fixtures/ariana.jpg',
		info: 'Singer',
		published: true,
	}, {
		user: admin,
		name: 'Charlie Puth',
		image: 'fixtures/puth.jpg',
		info: 'Singer',
		published: true,
	});

	const [albumAriana, albumPuth] = await Album.create({
		user: user,
		name: 'Thank U, next',
		artist: Ariana,
		outYear: 2019,
		image: 'fixtures/ariana-album.jpg',
		published: true,
	}, {
		user: admin,
		name: 'Voicenotes',
		artist: Puth,
		outYear: 2018,
		image: 'fixtures/puth-album.jpg',
		published: true,
	});

	await Track.create(
		{
			user: user,
			name: 'Imagine',
			album: albumAriana,
			duration: '3:32',
			trackNumber: 1,
			published: false,
		}, {
			user: user,
			name: 'Needy',
			album: albumAriana,
			duration: '2:51',
			trackNumber: 2,
			published: false,
		}, {
			user: admin,
			name: 'NASA',
			album: albumAriana,
			duration: '3:02',
			trackNumber: 3,
			published: true,
		},{
			user: admin,
			name: 'Attention',
			album: albumPuth,
			duration: '3:31',
			trackNumber: 2,
			published: true,
		}, {
			user: user,
			name: 'LA Girls',
			album: albumPuth,
			duration: '3:17',
			trackNumber: 3,
			published: true,
		}, {
			user: user,
			name: 'How Long',
			album: albumPuth,
			duration: '3:20',
			trackNumber: 4,
			published: true,
		},
	);

	await mongoose.connection.close();
};

run().catch(console.error);