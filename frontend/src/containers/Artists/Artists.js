import React, {useEffect} from 'react';
import {CircularProgress, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import Artist from "../../components/Artist/Artist";
import {clearPublish} from "../../store/actions/publishActions";

const useStyles = makeStyles({
	artistContainer: {
		marginTop: '20px'
	}
});

const Artists = () => {
	const dispatch = useDispatch();
	const artists = useSelector(state => state.artists.artists);
	const fetchLoading = useSelector(state => state.artists.fetchLoading);
	const error = useSelector(state => state.artists.error);

	const classes = useStyles();

	useEffect(() => {
		dispatch(fetchArtists());

		return () => {
			dispatch(clearPublish());
		}
	}, [dispatch]);

	return artists && error ? <Typography>{error.error}</Typography> : <Container >
			<Grid container direction="column" spacing={2}>
				<Grid item >
					<Grid item container direction="row" spacing={2} className={classes.artistContainer}>
						{fetchLoading ? (
							<Grid container justifyContent="center" alignItems="center">
								<Grid item>
									<CircularProgress/>
								</Grid>
							</Grid>
						) : artists.map(artist => (
							<Artist
								key={artist._id}
								id={artist._id}
								name={artist.name}
								image={artist.image}
								published={artist.published}
							/>
						))}
					</Grid>
				</Grid>
			</Grid>
		</Container>
};

export default Artists;