import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import ArtistForm from "../../components/ArtistForm/ArtistForm";
import {useDispatch, useSelector} from "react-redux";
import {createArtist} from "../../store/actions/artistsActions";
import {clearErrors} from "../../store/actions/clearErrorsActions";

const AddArtist = () => {
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);

	const onsubmit = artistData => {
		dispatch(createArtist(artistData));
	};

	useEffect(() => {
		return () => {
			dispatch(clearErrors());
		};
	}, [dispatch]);

	return user ?
		<>
			<Typography variant="h4">New Artist</Typography>
			<ArtistForm onSubmit={onsubmit}/>
		</> :
		<Typography variant="h5">Page not available</Typography>
};

export default AddArtist;