import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, CircularProgress, Grid, IconButton, Paper, Typography} from "@material-ui/core";
import {addToTrackHistory, deleteTrack, fetchTracks} from "../../store/actions/tracksActions";
import {fetchAlbum} from "../../store/actions/albumsActions";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {clearPublish, publish} from "../../store/actions/publishActions";
import DeleteIcon from '@material-ui/icons/Delete';
const queryString = require('query-string');

const Tracks = ({location}) => {
	const dispatch = useDispatch();
	const tracks = useSelector(state => state.tracks.tracks);
	const tracksLoading = useSelector(state => state.tracks.tracksLoading);
	const error = useSelector(state => state.tracks.error);
	const album = useSelector(state => state.albums.album);
	const user = useSelector(state => state.users.user);
	const publishLoading = useSelector(state => state.publish.publishLoading);
	const published = useSelector(state => state.publish.publish);

	useEffect(() => {
		dispatch(fetchTracks(location.search));
		dispatch(fetchAlbum(queryString.parse(location.search).album));

		return () => {
			dispatch(clearPublish());
		}
	}, [dispatch, location.search]);

	const clickOnTrack = id => {
		dispatch(addToTrackHistory({track_id: id}));
	};

	return tracks && error ?
		<>
			<Typography component="h1" variant="h4">{error.artist.name}</Typography>
			<Typography component="h2" variant="h3">{error.album.name}</Typography>
			<Typography variant="h6">{error.error}</Typography>
		</> :
		<Grid item>
			{album && album.artist ? <Typography variant="h4">{album.artist.name}</Typography> : <></>}
			{tracks && tracks[0] ? <Typography variant="subtitle1">{tracks[0].album.name}</Typography> : <></>}
			{tracksLoading ? (
				<Grid container justifyContent="center" alignItems="center">
					<Grid item>
						<CircularProgress/>
					</Grid>
				</Grid>
			) : tracks.map(track => (
				<Grid item xs={12} key={track._id}>
					<Paper component={Box} m={3} p={2}>
						<div onClick={() => clickOnTrack(track._id)}>
							<Typography variant="subtitle2">№ {track.trackNumber}</Typography>
							<Typography variant="h5">
								{track.name}
							</Typography>
							<Typography variant="subtitle2">
								{track.duration}
							</Typography>
						</div>
						{(user && user.role === 'admin') &&
							<>
								{(track.published === false && published === false) &&
								<ButtonWithProgress
									variant="contained"
									color="secondary"
									onClick={() => dispatch(publish('tracks', track._id))}
									loading={publishLoading}
									disabled={publishLoading}
								>
									Not published
								</ButtonWithProgress>
								}
								<IconButton onClick={() => dispatch(deleteTrack(track._id))}>
									<DeleteIcon/>
								</IconButton>
							</>
						}
					</Paper>
				</Grid>
			))}
		</Grid>
};

export default Tracks;