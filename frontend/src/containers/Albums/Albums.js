import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {CircularProgress, Container, Grid, Typography} from "@material-ui/core";
import Album from "../../components/Album/Album";
import {clearPublish} from "../../store/actions/publishActions";

const Albums = ({location}) => {
	const dispatch = useDispatch();
	const albums = useSelector(state => state.albums.albums);
	const albumLoading = useSelector(state => state.albums.albumsLoading);
	const error = useSelector(state => state.albums.error);

	useEffect(() => {
		dispatch(fetchAlbums(location.search));

		return () => {
			dispatch(clearPublish());
		}
	}, [dispatch, location]);

	return albums && (error && error.artist) ? (
		<>
			<Typography component="h1" variant="h4">{error.artist.name}</Typography>
			<Typography>{error.error}</Typography>
		</>
	) : <Container>
		<Grid>
			<Grid item>
				<Typography variant="h4">
					{albums && albums[0] ? albums[0].artist['name'] : <span>Albums not found</span>}
				</Typography>
			</Grid>
			<Grid item>
				<Grid item container direction="row" spacing={2}>
					{albumLoading ? (
						<Grid container justifyContent="center" alignItems="center">
							<Grid item>
								<CircularProgress/>
							</Grid>
						</Grid>
					) : albums.map(album => (
						<Album
							key={album._id}
							id={album._id}
							name={album.name}
							image={album.image}
							outYear={album.outYear}
							artist={album.artist.name}
							published={album.published}
						/>
					))}
				</Grid>
			</Grid>
		</Grid>
	</Container>
};

export default Albums;