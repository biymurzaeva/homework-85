import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import AlbumForm from "../../components/AlbumForm/AlbumForm";
import {createAlbum} from "../../store/actions/albumsActions";
import {clearErrors} from "../../store/actions/clearErrorsActions";

const AddAlbum = () => {
	const dispatch = useDispatch();
	const artists = useSelector(state => state.artists.artists);
	const user = useSelector(state => state.users.user);

	useEffect(() => {
		dispatch(fetchArtists());

		return () => {
			dispatch(clearErrors());
		};
	}, [dispatch]);

	const onSubmit = albumData => {
		dispatch(createAlbum(albumData));
	};

	return user ?
		<>
			<Typography variant="h5">Add new album</Typography>
			<AlbumForm artists={artists} onSubmit={onSubmit}/>
		</> :
		<Typography variant="h5">Page not available</Typography>
};

export default AddAlbum;