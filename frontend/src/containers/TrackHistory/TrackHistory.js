import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Moment from 'moment';
import {fetchTrackHistory} from "../../store/actions/trackHistoryActions";
import {makeStyles, Typography} from "@material-ui/core";

const useStyles = makeStyles({
	trackBlock: {
		border: '1px solid #ccc',
		borderRadius: '4px',
		padding: '15px',
		marginBottom: '25px',
	},
});
const TrackHistory = () => {
	const dispatch = useDispatch();
	const tracks = useSelector(state => state.trackHistory.tracks);
	const error = useSelector(state => state.trackHistory.error);

	const classes = useStyles();

	useEffect(() => {
		dispatch(fetchTrackHistory());
	}, [dispatch]);

	return tracks && (error && error.error) ? <Typography>{error.error}</Typography> :
		<>
			{tracks.map(track => (
				<div className={classes.trackBlock} key={track.track_id._id}>
					<Typography variant="h5">{track.track_id.album.artist.name}</Typography>
					<Typography variant="h6">{track.track_id.name}</Typography>
					<Typography variant="subtitle1">{Moment(track.datetime).format('DD.MM.YYYY HH:mm:ss')}</Typography>
				</div>
			))}
		</>
};

export default TrackHistory;