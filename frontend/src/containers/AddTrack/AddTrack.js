import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createTrack} from "../../store/actions/tracksActions";
import {getAlbums} from "../../store/actions/albumsActions";
import TrackForm from "../../components/TrackForm/TrackForm";
import {clearErrors} from "../../store/actions/clearErrorsActions";

const AddTrack = () => {
	const dispatch = useDispatch();
	const albums = useSelector(state => state.albums.fullAlbums);
	const user = useSelector(state => state.users.user);

	useEffect(() => {
		dispatch(getAlbums());

		return () => {
			dispatch(clearErrors());
		}
	}, [dispatch]);

	const onSubmit = async trackData => {
		await dispatch(createTrack({...trackData}));
	};

	return user ?
		<>
			<Typography variant="h5">New track</Typography>
			<TrackForm albums={albums} onSubmit={onSubmit} />
		</> :
		<Typography variant="h5">Page not available</Typography>
};

export default AddTrack;