import {Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

const App = () => (
	<Layout>
		<Switch>
			<Route path="/" exact component={Artists}/>
			<Route path="/albums/new" component={AddAlbum}/>
			<Route path="/albums" component={Albums}/>
			<Route path="/tracks/new" component={AddTrack}/>
			<Route path="/tracks" component={Tracks}/>
			<Route path="/register" component={Register}/>
			<Route path="/login" component={Login}/>
			<Route path="/track-history" component={TrackHistory}/>
			<Route path="/artists/new" component={AddArtist}/>
		</Switch>
	</Layout>
);

export default App;
