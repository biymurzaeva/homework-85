import React from 'react';
import {Card, CardHeader, CardMedia, Grid, IconButton, makeStyles} from "@material-ui/core";
import musicImage from '../../assets/images/mp3.jpg';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import DeleteIcon from "@material-ui/icons/Delete";
import {deleteArtist} from "../../store/actions/artistsActions";
import {publish} from "../../store/actions/publishActions";

const useStyle = makeStyles({
	card: {
		height: '100%'
	},
	media: {
		height: 0,
		paddingTop: '56.25%'
	}
});

const Artist = props => {
	const classes = useStyle();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const publishLoading = useSelector(state => state.publish.publishLoading);
	const published = useSelector(state => state.publish.publish);

	let cardImage = musicImage;

	if (props.image) {
		cardImage = apiURL + '/' + props.image;
	}

	return (
		<Grid item xs={12} sm={6} md={4} lg={3}>
			<Card
				className={classes.card}
				component={Link}
				to={'/albums?artist=' + props.id}
			>
				<CardMedia
					image={cardImage}
					className={classes.media}
				/>
				<CardHeader title={props.name}/>
			</Card>
			{(user && user.role === 'admin') &&
				<>
					<IconButton	onClick={() => dispatch(deleteArtist(props.id))}>
						<DeleteIcon/>
					</IconButton>
					{(props.published === false && published === false) &&
					<ButtonWithProgress
						fullWidth
						variant="contained"
						color="secondary"
						onClick={() => dispatch(publish('artists', props.id))}
						loading={publishLoading}
						disabled={publishLoading}
					>
						Not published
					</ButtonWithProgress>
					}
				</>
			}
		</Grid>
	);
};

export default Artist;