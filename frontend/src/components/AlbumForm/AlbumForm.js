import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const AlbumForm = ({onSubmit, artists}) => {
	const classes = useStyles();
	const error = useSelector(state => state.albums.createAlbumError);

	const [album, setAlbum] = useState({
		name: '',
		artist: '',
		image: null,
		outYear: 0,
	});

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();
		Object.keys(album).forEach(key => {
			formData.append(key, album[key]);
		});

		onSubmit(formData);
	};

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setAlbum(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setAlbum(prevState => ({...prevState, [name]: file}));
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<FormElement
				select
				options={artists}
				label="Artist"
				name="artist"
				value={album.artist}
				onChange={inputChangeHandler}
			/>
			<FormElement
				label="Name"
				name="name"
				value={album.name}
				onChange={inputChangeHandler}
				error={getFieldError('name')}
			/>
			<FormElement
				label="Release date"
				name="outYear"
				value={album.outYear}
				onChange={inputChangeHandler}
				error={getFieldError('outYear')}
			/>
			<Grid item xs>
				<TextField
					type="file"
					name="image"
					onChange={fileChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Add</Button>
			</Grid>
		</Grid>
	);
};

export default AlbumForm;