import React from 'react';
import {Grid, IconButton, makeStyles, Typography} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/mp3.jpg';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {publish} from "../../store/actions/publishActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import DeleteIcon from "@material-ui/icons/Delete";
import {deleteAlbum} from "../../store/actions/albumsActions";

const useStyle = makeStyles({
	albumItem: {
		display: 'block',
		marginTop: '15px',
		textDecoration: 'none',
	},
	albumImg: {
		maxWidth: '250px',
		maxHeight: '250px',
	},
	albumName: {
		textTransform: 'uppercase',
	}
});

const Album = props => {
	const classes = useStyle();
	const dispatch = useDispatch();
	const user = useSelector(state => state.users.user);
	const publishLoading = useSelector(state => state.publish.publishLoading);
	const published = useSelector(state => state.publish.publish);

	let albumImage = imageNotAvailable;

	if (props.image) {
		albumImage = apiURL + '/' + props.image;
	}

	return (
		<Grid item xs={12} sm={6} md={4} lg={3}>
			<Grid
				item
				xs
				className={classes.albumItem}
				component={Link}
				to={'/tracks?album=' + props.id}
			>
				<img src={albumImage} alt="Album" className={classes.albumImg}/>
				<Typography variant="subtitle1" className={classes.albumName}>
					{props.name}
				</Typography>
				{props.outYear ? <Typography variant="subtitle2">{props.outYear}</Typography> : <></>}
			</Grid>
			{(user && user.role === 'admin') &&
				<>
					{(props.published === false && published === false) &&
					<ButtonWithProgress
						fullWidth
						variant="contained"
						color="secondary"
						onClick={() => dispatch(publish('albums', props.id))}
						loading={publishLoading}
						disabled={publishLoading}
					>
						Not published
					</ButtonWithProgress>}
					<IconButton onClick={() => dispatch(deleteAlbum(props.id))}>
						<DeleteIcon/>
					</IconButton>
				</>
			}
		</Grid>
	);
};

export default Album;