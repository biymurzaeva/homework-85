import React, {useState} from 'react';
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const ArtistForm = ({onSubmit}) => {
	const classes = useStyles();
	const error = useSelector(state => state.artists.createError);

	const [artist, setArtist] = useState({
		name: '',
		image: null,
		info: '',
	});

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();
		Object.keys(artist).forEach(key => {
			formData.append(key, artist[key]);
		});

		onSubmit(formData);
	};

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setArtist(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setArtist(prevState => ({...prevState, [name]: file}));
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<FormElement
				label="Name"
				name="name"
				value={artist.name}
				onChange={inputChangeHandler}
				error={getFieldError('name')}
			/>
			<FormElement
				multiline
				rows={4}
				label="Information"
				name="info"
				value={artist.info}
				onChange={inputChangeHandler}
			/>
			<Grid item xs>
				<TextField
					type="file"
					name="image"
					onChange={fileChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Add</Button>
			</Grid>
		</Grid>
	);
};

export default ArtistForm;