import React from 'react';
import {useDispatch} from "react-redux";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {facebookAppId} from "../../../config";
import {Button} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import {facebookLogin} from "../../../store/actions/usersActions";

const FacebookLogin = () => {
	const dispatch = useDispatch();

	const facebookResponse = response => {
		dispatch(facebookLogin(response));
	};

	return (
		<FacebookLoginButton
			appId={facebookAppId}
			fields="name,email,picture"
			render={props => (
				<Button
					fullWidth
					variant="outlined"
					color="primary"
					startIcon={<FacebookIcon/>}
					onClick={props.onClick}
				>
					Login with facebook
				</Button>
			)}
			callback={facebookResponse}
		/>
	);
};

export default FacebookLogin;