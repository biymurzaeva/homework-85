import React from 'react';
import {AppBar, Button, Grid, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";

const useStyles = makeStyles(theme => ({
  mainLink: {
    color: "inherit",
    textDecoration: 'none',
    '$:hover': {
      color: 'inherit'
    }
  },
  staticToolbar: {
    marginBottom: theme.spacing(2)
  }
}));

const AppToolbar = () => {
	const classes = useStyles();
	const user = useSelector(state => state.users.user);

  return (
    <>
      <AppBar position="fixed">
        <Toolbar>
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item>
              <Typography variant="h6">
                <Link to="/" className={classes.mainLink}>Music App</Link>
              </Typography>
            </Grid>
            <Grid item>
	            {user ? (
	            	<UserMenu user={user}/>
	            ) : (
								<AnonymousMenu/>
	            )}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <Toolbar className={classes.staticToolbar}/>
	    {user && <Grid item>
		    <Button color="primary" component={Link} to="/artists/new">Add new artist</Button>
		    <Button color="primary" component={Link} to="/albums/new">Add new album</Button>
		    <Button color="primary" component={Link} to="/tracks/new">Add new track</Button>
	    </Grid>
	    }
    </>
  );
};

export default AppToolbar;