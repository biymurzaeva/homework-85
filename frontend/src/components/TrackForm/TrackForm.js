import React, {useState} from 'react';
import {Button, Grid, makeStyles} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
}));

const TrackForm = ({onSubmit, albums}) => {
	const classes = useStyles();
	const error = useSelector(state => state.tracks.createTrackError);

	const [track, setTrack] = useState({
		name: '',
		album: '',
		duration: '',
		trackNumber: 0,
	});

	const submitFormHandler = e => {
		e.preventDefault();
		onSubmit(track);
	};

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setTrack(prevState => ({...prevState, [name]: value}));
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	return albums && (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<FormElement
				select
				options={albums}
				label="Albums"
				name="album"
				value={track.album}
				onChange={inputChangeHandler}
				error={getFieldError('album')}
			/>
			<FormElement
				label="Name"
				name="name"
				value={track.name}
				onChange={inputChangeHandler}
				error={getFieldError('name')}
			/>
			<FormElement
				label="Duration"
				name="duration"
				value={track.duration}
				onChange={inputChangeHandler}
			/>
			<FormElement
				label="Track number"
				name="trackNumber"
				value={track.trackNumber}
				onChange={inputChangeHandler}
				error={getFieldError('trackNumber')}
			/>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Add track</Button>
			</Grid>
		</Grid>
	);
};

export default TrackForm;