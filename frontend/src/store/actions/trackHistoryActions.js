import axiosApi from "../../axiosApi";

export const FETCH_TRACK_HISTORY_REQUEST = 'FETCH_TRACK_HISTORY_REQUEST';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';

export const fetchTrackHistoryRequest = () => ({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTrackHistorySuccess = tracks => ({type: FETCH_TRACK_HISTORY_SUCCESS, payload: tracks});
export const fetchTrackHistoryFailure = error => ({type: FETCH_TRACK_HISTORY_FAILURE, payload: error});

export const fetchTrackHistory = () => {
	return async (dispatch, getState) => {
		try {
			const headers = {
				'Authorization': getState().users.user && getState().users.user.token
			};

			dispatch(fetchTrackHistoryRequest());
			const response = await axiosApi.get('/track_history', {headers});
			dispatch(fetchTrackHistorySuccess(response.data));
		} catch (error) {
			dispatch(fetchTrackHistoryFailure(error.response.data));
		}
	};
};