import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';

export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_FAILURE = 'CREATE_ARTIST_FAILURE';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
export const fetchArtistsFailure = error => ({type: FETCH_ARTISTS_FAILURE, payload: error});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistFailure = error => ({type: CREATE_ARTIST_FAILURE, payload: error});

export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = id => ({type: DELETE_ARTIST_SUCCESS, payload: id});
export const deleteArtistFailure = error => ({type: DELETE_ARTIST_FAILURE, payload: error});

export const fetchArtists = () => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(fetchArtistsRequest());
			const response = await axiosApi.get('/artists', {headers});
			dispatch(fetchArtistsSuccess(response.data));
		} catch (error) {
			dispatch(fetchArtistsFailure(error.response.data));
		}
	};
};

export const createArtist = artistData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user.token,
		};

		try {
			dispatch(createArtistRequest());
			await axiosApi.post('/artists', artistData, {headers});
			dispatch(createArtistSuccess());
			dispatch(historyPush('/'));
			toast.success('Added a new artist');
		} catch (error) {
			dispatch(createArtistFailure(error.response.data));
		}
	};
};

export const deleteArtist = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user.token,
		};

		try {
			dispatch(deleteArtistRequest());
			await axiosApi.delete(`/artists/${id}`, {headers});
			dispatch(deleteArtistSuccess(id));
			toast.success('Artist deleted');
		} catch (error) {
			dispatch(deleteArtistFailure(error.response.data));
			toast.error('Artist not deleted');
		}
	};
};
