import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";

export const PUBLISH_REQUEST = 'PUBLISH_REQUEST';
export const PUBLISH_SUCCESS = 'PUBLISH_SUCCESS';
export const PUBLISH_FAILURE = 'PUBLISH_FAILURE';

export const CLEAR_PUBLISH = 'CLEAR_PUBLISH';

export const publishRequest = () => ({type: PUBLISH_REQUEST});
export const publishSuccess = () => ({type: PUBLISH_SUCCESS});
export const publishFailure = error => ({type: PUBLISH_FAILURE, payload: error});

export const clearPublish = () => ({type: CLEAR_PUBLISH});

export const publish = (path, id) => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user.token,
		};

		try {
			dispatch(publishRequest());
			await axiosApi.put(`/${path}/${id}/publish`, {}, {headers});
			dispatch(publishSuccess());
			toast.success('Published!');
		} catch (error) {
			dispatch(publishFailure(error));
			toast.error('Something went wrong');
		}
	};
};