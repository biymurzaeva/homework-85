import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "./historyActions";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const FETCH_ALBUM_REQUEST = 'FETCH_ALBUM_REQUEST';
export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS';
export const FETCH_ALBUM_FAILURE = 'FETCH_ALBUM_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const GET_ALBUMS_REQUEST = 'GET_ALBUMS_REQUEST';
export const GET_ALBUMS_SUCCESS = 'GET_ALBUMS_SUCCESS';
export const GET_ALBUMS_FAILURE = 'GET_ALBUMS_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
export const fetchAlbumsFailure = error => ({type: FETCH_ALBUMS_FAILURE, payload: error});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = error => ({type: CREATE_ALBUM_FAILURE, payload: error});

export const fetchAlbumRequest = () => ({type: FETCH_ALBUM_REQUEST});
export const fetchAlbumSuccess = album => ({type: FETCH_ALBUM_SUCCESS, payload: album});
export const fetchAlbumFailure = error => ({type: FETCH_ALBUM_FAILURE, payload: error});

export const getAlbumsRequest = () => ({type: GET_ALBUMS_REQUEST});
export const getAlbumsSuccess = albums => ({type: GET_ALBUMS_SUCCESS, payload: albums});
export const getAlbumsFailure = error => ({type: GET_ALBUMS_FAILURE, payload: error});

export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = id => ({type: DELETE_ALBUM_SUCCESS, payload: id});
export const deleteAlbumFailure = error => ({type: DELETE_ALBUM_FAILURE, payload: error});

export const fetchAlbums = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(fetchAlbumsRequest());
			const response = await axiosApi.get('/albums' + id, {headers});
			dispatch(fetchAlbumsSuccess(response.data));
		} catch (error) {
			dispatch(fetchAlbumsFailure(error.response.data));
		}
	};
};

export const fetchAlbum = id => {
	return async dispatch => {
		try {
			dispatch(fetchAlbumRequest());
			const album = await axiosApi.get('/albums/' + id);
			dispatch(fetchAlbumSuccess(album.data));
		} catch (error) {
			dispatch(fetchAlbumFailure(error.response.data));
		}
	};
};

export const createAlbum = albumData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(createAlbumRequest());
			await axiosApi.post('/albums', albumData, {headers})
			dispatch(createAlbumSuccess());
			dispatch(historyPush('/'));
			toast.success('Created a new album');
		} catch (error) {
			dispatch(createAlbumFailure(error.response.data));
		}
	};
};

export const getAlbums = () => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(getAlbumsRequest());
			const response = await axiosApi.get('/albums', {headers});
			dispatch(getAlbumsSuccess(response.data));
		} catch (error) {
			dispatch(getAlbumsFailure(error.response.data));
		}
	};
};

export const deleteAlbum = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(deleteAlbumRequest());
			await axiosApi.delete(`/albums/${id}`, {headers});
			dispatch(deleteAlbumSuccess(id));
		} catch (error) {
			dispatch(deleteAlbumFailure(error.response.data));
		}
	};
};