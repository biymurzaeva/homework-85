import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';

export const ADD_TO_TRACK_HISTORY_REQUEST = 'ADD_TO_TRACK_HISTORY_REQUEST';
export const ADD_TO_TRACK_HISTORY_SUCCESS = 'ADD_TO_TRACK_HISTORY_SUCCESS';
export const ADD_TO_TRACK_HISTORY_FAILURE = 'ADD_TO_TRACK_HISTORY_FAILURE';

export const CREATE_TRACK_REQUEST = 'CREATE_TRACK_REQUEST';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_FAILURE = 'CREATE_TRACK_FAILURE';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';

export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, payload: tracks});
export const fetchTracksFailure = error => ({type: FETCH_TRACKS_FAILURE, payload: error});

export const addToTrackHistoryRequest = () => ({type: ADD_TO_TRACK_HISTORY_REQUEST});
export const addToTrackHistorySuccess = () => ({type: ADD_TO_TRACK_HISTORY_SUCCESS});
export const addToTrackHistoryFailure = error => ({type: ADD_TO_TRACK_HISTORY_FAILURE, payload: error});

export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = error => ({type: CREATE_TRACK_FAILURE, payload: error});

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = id => ({type: DELETE_TRACK_SUCCESS, payload: id});
export const deleteTrackFailure = error => ({type: DELETE_TRACK_FAILURE, payload: error});

export const fetchTracks = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token
		};

		try {
			dispatch(fetchTracksRequest());
			const response = await axiosApi.get('/tracks' + id, {headers});
			dispatch(fetchTracksSuccess(response.data));
		} catch (error) {
			dispatch(fetchTracksFailure(error.response.data));
		}
	};
};

export const addToTrackHistory = id => {
	return async (dispatch, getState) => {
		try {
			const headers = {
				'Authorization': getState().users.user && getState().users.user.token
			};

			dispatch(addToTrackHistoryRequest());
			await axiosApi.post('/track_history', id, {headers});
			dispatch(addToTrackHistorySuccess());
		} catch (error) {
			if (error) {
				dispatch(historyPush('/register'));
				toast.error('You need to register');
			}

			dispatch(addToTrackHistoryFailure(error));
		}
	}
};

export const createTrack = trackData => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(createTrackRequest());
			await axiosApi.post('/tracks', trackData, {headers})
			dispatch(createTrackSuccess());
			dispatch(historyPush('/'));
			toast.success('Created a new track');
		} catch (error) {
			dispatch(createTrackFailure(error.response.data));
		}
	};
};

export const deleteTrack = id => {
	return async (dispatch, getState) => {
		const headers = {
			'Authorization': getState().users.user && getState().users.user.token,
		};

		try {
			dispatch(deleteTrackRequest());
			await axiosApi.delete(`/tracks/${id}`, {headers});
			dispatch(deleteTrackSuccess(id));
			toast.success('Track deleted');
		} catch (error) {
			dispatch(deleteTrackFailure(error.response.data));
			toast.error('Track not deleted');
		}
	};
};