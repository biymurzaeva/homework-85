import {CLEAR_PUBLISH, PUBLISH_FAILURE, PUBLISH_REQUEST, PUBLISH_SUCCESS} from "../actions/publishActions";

const initialState = {
	publish: false,
	publishLoading: false,
	publishError: null,
};

const publishReducer = (state= initialState, action) => {
	switch (action.type) {
		case PUBLISH_REQUEST:
			return {...state, publishLoading: true};
		case PUBLISH_SUCCESS:
			return {...state, publishLoading: false, publishError: null, publish: true};
		case PUBLISH_FAILURE:
			return {...state, publishLoading: false, publishError: action.payload};
		case CLEAR_PUBLISH:
			return {...state, publish: false};
		default:
			return state;
	}
};

export default publishReducer;