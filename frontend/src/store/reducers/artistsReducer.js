import {
	CREATE_ARTIST_FAILURE,
	CREATE_ARTIST_REQUEST,
	CREATE_ARTIST_SUCCESS,
	DELETE_ARTIST_FAILURE,
	DELETE_ARTIST_REQUEST,
	DELETE_ARTIST_SUCCESS,
	FETCH_ARTISTS_FAILURE,
	FETCH_ARTISTS_REQUEST,
	FETCH_ARTISTS_SUCCESS,
} from "../actions/artistsActions";
import {CLEAR_ERRORS} from "../actions/clearErrorsActions";

const initialState = {
	fetchLoading: false,
	artists: [],
	error: null,
	createLoading: false,
	createError: null,
	deleteArtistLoading: false,
	deleteArtistError: null,
};

const artistsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ARTISTS_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_ARTISTS_SUCCESS:
			return {...state,  fetchLoading: false, error: null, artists: action.payload};
		case FETCH_ARTISTS_FAILURE:
			return {...state, fetchLoading: false, error: action.payload};
		case CREATE_ARTIST_REQUEST:
			return {...state, createLoading: true};
		case CREATE_ARTIST_SUCCESS:
			return {...state, createLoading: false, createError: null};
		case CREATE_ARTIST_FAILURE:
			return {...state, createLoading: false, createError: action.payload};
		case CLEAR_ERRORS:
			return {...state, createError: null};
		case DELETE_ARTIST_REQUEST:
			return {...state, deleteArtistLoading: true};
		case DELETE_ARTIST_SUCCESS:
			return {
				...state,
				deleteArtistLoading: false,
				deleteArtistError: null,
				artists: state.artists.filter(a => a._id !== action.payload),
			};
		case DELETE_ARTIST_FAILURE:
			return {...state, deleteArtistLoading: false, deleteArtistError: action.payload};
		default:
			return state;
	}
};

export default artistsReducer;