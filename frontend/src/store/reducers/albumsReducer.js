import {
	CREATE_ALBUM_FAILURE,
	CREATE_ALBUM_REQUEST,
	CREATE_ALBUM_SUCCESS,
	DELETE_ALBUM_FAILURE,
	DELETE_ALBUM_REQUEST,
	DELETE_ALBUM_SUCCESS,
	FETCH_ALBUM_FAILURE,
	FETCH_ALBUM_REQUEST,
	FETCH_ALBUM_SUCCESS,
	FETCH_ALBUMS_FAILURE,
	FETCH_ALBUMS_REQUEST,
	FETCH_ALBUMS_SUCCESS,
	GET_ALBUMS_FAILURE,
	GET_ALBUMS_REQUEST,
	GET_ALBUMS_SUCCESS
} from "../actions/albumsActions";
import {CLEAR_ERRORS} from "../actions/clearErrorsActions";

const initialState = {
	albumsLoading: false,
	albums: [],
	error: null,
	album: {},
	albumLoading: false,
	createLoading: false,
	createAlbumError: null,
	fullAlbums: null,
	getFullAlbumsError: null,
	getFullAlbumsLoading: null,
	deleteAlbumLoading: false,
	deleteAlbumError: null,
};

const albumsReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ALBUMS_REQUEST:
			return {...state, albumsLoading: true};
		case FETCH_ALBUMS_SUCCESS:
			return {...state, albumsLoading: false, error: null, albums: action.payload};
		case FETCH_ALBUMS_FAILURE:
			return {...state, albumsLoading: false, error: action.payload};
		case FETCH_ALBUM_REQUEST:
			return {...state, albumLoading: true};
		case FETCH_ALBUM_SUCCESS:
			return {...state, albumLoading: false, album: action.payload};
		case FETCH_ALBUM_FAILURE:
			return {...state, albumLoading: false, error: action.payload};
		case CREATE_ALBUM_REQUEST:
			return {...state, createLoading: true};
		case CREATE_ALBUM_SUCCESS:
			return {...state, createLoading: false, createAlbumError: null};
		case CREATE_ALBUM_FAILURE:
			return {...state, createLoading: false, createAlbumError: action.payload};
		case GET_ALBUMS_REQUEST:
			return {...state, getFullAlbumsLoading: true};
		case GET_ALBUMS_SUCCESS:
			return {...state, getFullAlbumsLoading: false, fullAlbums: action.payload, getFullAlbumsError: null};
		case GET_ALBUMS_FAILURE:
			return {...state, getFullAlbumsLoading: false, getFullAlbumsError: action.payload};
		case CLEAR_ERRORS:
			return {...state, createAlbumError: null};
		case DELETE_ALBUM_REQUEST:
			return {...state, deleteAlbumLoading: true};
		case DELETE_ALBUM_SUCCESS:
			return {
				...state,
				deleteAlbumLoading: false,
				deleteAlbumError: null,
				albums: state.albums.filter(a => a._id !== action.payload),
			};
		case DELETE_ALBUM_FAILURE:
			return {...state, deleteAlbumLoading: false, deleteAlbumError: action.payload};
		default:
			return state;
	}
};

export default albumsReducer;