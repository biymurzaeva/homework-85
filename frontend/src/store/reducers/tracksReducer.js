import {
	ADD_TO_TRACK_HISTORY_FAILURE,
	ADD_TO_TRACK_HISTORY_REQUEST,
	ADD_TO_TRACK_HISTORY_SUCCESS,
	CREATE_TRACK_FAILURE,
	CREATE_TRACK_REQUEST,
	CREATE_TRACK_SUCCESS, DELETE_TRACK_FAILURE, DELETE_TRACK_REQUEST, DELETE_TRACK_SUCCESS,
	FETCH_TRACKS_FAILURE,
	FETCH_TRACKS_REQUEST,
	FETCH_TRACKS_SUCCESS
} from "../actions/tracksActions";
import {CLEAR_ERRORS} from "../actions/clearErrorsActions";

const initialState = {
	tracksLoading: false,
	tracks: [],
	error: null,
	addLoading: false,
	addingError: null,
	createLoading: false,
	createTrackError: null,
	deleteError: null,
	deleteLoading: false,
};

const tracksReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TRACKS_REQUEST:
			return {...state, tracksLoading: true};
		case FETCH_TRACKS_SUCCESS:
			return {...state, tracksLoading: false, error: null, tracks: action.payload};
		case FETCH_TRACKS_FAILURE:
			return {...state, tracksLoading: false, error: action.payload};
		case ADD_TO_TRACK_HISTORY_REQUEST:
			return {...state, addLoading: true}
		case ADD_TO_TRACK_HISTORY_SUCCESS:
			return {...state, addLoading: false,  addingError: null};
		case ADD_TO_TRACK_HISTORY_FAILURE:
			return {...state, addLoading: false, addingError: action.payload};
		case CREATE_TRACK_REQUEST:
			return {...state, createLoading: true}
		case CREATE_TRACK_SUCCESS:
			return {...state, createLoading: false, createTrackError: null};
		case CREATE_TRACK_FAILURE:
			return {...state, createLoading: false, createTrackError: action.payload};
		case CLEAR_ERRORS:
			return {...state, createTrackError: null};
		case DELETE_TRACK_REQUEST:
			return {...state, deleteLoading: true};
		case DELETE_TRACK_SUCCESS:
			return {
				...state,
				deleteLoading: false,
				deleteError: null,
				tracks: state.tracks.filter(t => t._id !== action.payload),
			};
		case DELETE_TRACK_FAILURE:
			return {...state, deleteLoading: false, deleteError: action.payload};
		default:
			return state;
	}
};

export default tracksReducer;