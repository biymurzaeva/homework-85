import {
	FETCH_TRACK_HISTORY_FAILURE,
	FETCH_TRACK_HISTORY_REQUEST, FETCH_TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
	loading: false,
	tracks: [],
	error: null
};

const trackHistoryReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TRACK_HISTORY_REQUEST:
			return {...state, loading: true};
		case FETCH_TRACK_HISTORY_SUCCESS:
			return {...state,  loading: false, error: null, tracks: action.payload};
		case FETCH_TRACK_HISTORY_FAILURE:
			return {...state, loading: false, error: action.payload};
		default:
			return state;
	}
};

export default trackHistoryReducer;