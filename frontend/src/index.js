import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import history from "./history";
import {Router} from "react-router-dom"
import artistsReducer from "./store/reducers/artistsReducer";
import albumsReducer from "./store/reducers/albumsReducer";
import tracksReducer from "./store/reducers/tracksReducer";
import {createTheme, MuiThemeProvider} from "@material-ui/core";
import usersReducer from "./store/reducers/usersReduser";
import trackHistoryReducer from "./store/reducers/trackHistoryReducer";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import publishReducer from "./store/reducers/publishReducer";

const saveToLocalStorage = state => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem('musicState', serializedState);
	} catch (error) {
		console.log('Could not save state');
	}
};

const loadFromLocalStorage = () => {
	try {
		const serializedState = localStorage.getItem('musicState');

		if (serializedState === null) {
			return undefined;
		}

		return JSON.parse(serializedState);
	} catch (error) {
		return undefined;
	}
};

const rootReducer = combineReducers({
	artists: artistsReducer,
	albums: albumsReducer,
	tracks: tracksReducer,
	users: usersReducer,
	trackHistory: trackHistoryReducer,
	publish: publishReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
	rootReducer,
	persistedState,
	composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
	saveToLocalStorage({
		users: store.getState().users,
	});
});

const theme = createTheme({
	props: {
		MuiTextField: {
			variant: "outlined",
			fullWidth: true,
		},
	},
});

const app = (
	<Provider store={store}>
		<Router history={history}>
			<MuiThemeProvider theme={theme}>
				<ToastContainer/>
				<App/>
			</MuiThemeProvider>
		</Router>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
